# README #

Simple program generating jugging patterns in swap notation.

### What is this repository for? ###

* Demonstration of efficient programming in Python
* 0.2: translated from python2/dutch to python3/english

### How do I get set up? ###

* download, run once, look at the code

### Contribution guidelines ###

* Any comments on this code are welcome
* if you like to use argparse, or add a feature for the number of balls, contact me

### Who do I talk to? ###

* jnebos at the famous google mail server