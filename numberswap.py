#!python3
"""Generate number swap notation for juggling patterns
Generates all patterns given maximum pattern length and height.
Ideally, you would be able to restrict the number of balls too.
"""

from itertools import permutations


def genPermutations(length):
    """Genrate permutations in reverse order.
    Nonrecursive, for a change (which is also faster).
    This algorithm isn't used anymore; it is kept for reference
    Algoritm:
        take previous permutation
        find last low-to-high change (after which they go up)
        find next value for the low number in the following numbers
        (find the lowest higher value)
        fill that in by swapping
        complete by adding the rest, sorted (which is just the reverse)
    Note: it returns the same list each time;
    make sure you take a copy if you need that
    """
    if length < 2:
        if length == 1:
            yield [0]
        return
    from bisect import bisect
    # highest permutation
    perm = list(range(length-1, -1, -1))
    while True:
        yield perm
        # get the tail (longest list of increasing numbers at end)
        # this loop most often doesn't do anything
        index = length-2
        while perm[index] < perm[index+1]:
            if not index:
                return
            index -= 1
        # find what this number should become
        # that is, find next higher number in the tail
        nextIndex = bisect(perm, perm[index], index+1, length)-1
        # build new array: swap number with next, reverse tail
        perm[index], perm[nextIndex] = perm[nextIndex], perm[index]
        perm[index+1:] = perm[index+1:][::-1]


def revPermutations(length):
    """Generate permutations in reverse order,
    using the fast itertools way.
    """
    return permutations(reversed(range(length)))

def isLowestRotation(lst):
    """Check if a list is the lowest of its rotated variants
    """
    lowestDigit = min(lst)
    # simple check
    if lst[0] > lowestDigit:
        return False
    # get rotations starting with lowest digit
    rotationIndex = 0
    while True:
        # find next location of minimum
        try:
            rotationIndex = lst.index(lowestDigit, rotationIndex+1)
        except ValueError:
            # no more locations: this must be the lowest
            return True
        if lst[rotationIndex:]+lst[:rotationIndex] < lst:
            # this one is lower, so it isn't the lowest
            return False


def makePatterns(maxLengte, maxHeight):
    """Print all juggling swap codes up to given length and height"""
    print('Permutation, variants by max height')
    for patternLength in range(1, maxLengte+1):
        # generate patterns of this length
        for perm in revPermutations(patternLength):
            # convert pattern to distances
            swapcode = [(p-i) % patternLength for i,p in enumerate(perm)]
            if isLowestRotation(swapcode):
                # generate
                printVariants(swapcode, maxHeight, perm)


def printVariants(swapcode, maxHeight, perm, balls=None):
    """generate variants of swap code of given height
    assumes the swapcode given is the lowest rotation, and keeps it that way
    """
    patternLength = len(swapcode)
    # collect patterns by height 2,3,...
    patternsByHeight = [[] for i in range(2, maxHeight)]
    while True:
        # remove repeating patterns
        for divisor in range(1, patternLength//2+1):
            if patternLength % divisor:
                continue
            if swapcode[divisor:] == swapcode[:-divisor]:
                # this one repeats: skip
                break
        else:
            if 2 <= max(swapcode) < maxHeight and isLowestRotation(swapcode):
                # patterns doesn't repeat: add at proper height
                patternsByHeight[max(swapcode)-2].append(
                    ''.join(map(repr, swapcode)))
        # increase numbers (add a ball to the pattern)
        # find last increasable number
        index = patternLength
        while index:
            index -= 1
            if swapcode[index]+patternLength < maxHeight:
                # we can increase this value: do that now
                break
            # set this one to minimal, because we are increasing an earlier one
            swapcode[index] %= patternLength
        else:
            # no more increasing is possible
            break
        # increase the relevant value
        swapcode[index] += patternLength
    # sort and filter
    if balls is None:
        for patterns in patternsByHeight:
            patterns.sort()
    else:
        properSum = balls * len(swapcode)
        patternsByHeight = [
            sorted(pattern
                for pattern in patterns
                if sum(pattern) == properSum)
            for patterns in patternsByHeight]
    margin = ''.join(map(repr, perm)).rjust(7)
    for line in range(max(list(map(len, patternsByHeight)))):
        print(margin, end=' ')
        for patterns in patternsByHeight:
            if line < len(patterns):
                print(patterns[line].rjust(7), end=' ')
            else:
                print(' '*7, end=' ')
        print()
        margin = ' '*7


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 3:
        makePatterns(4, 6)
        print("Usage: %s <max length> <max height>" % sys.argv[0])
    else:
        makePatterns(int(sys.argv[1]), int(sys.argv[2])+1)
